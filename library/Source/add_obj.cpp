
#include "add_obj.h"
#include "load_model.h"

#include "random.h"
#include "collision.h"

AddObj* AddObj::instance = nullptr;

AddObj::AddObj(ID3D11Device* device)
{
}

AddObj::~AddObj()
{
}

void AddObj::SetPlayer(const VECTOR3& scale, const VECTOR3& angle, const VECTOR3& position)
{
	playerMove = std::make_shared<Player>();
	playerObj = std::make_unique<Obj3D>();
	playerObj->SetModelResource(LoadModel::GetInstance().GetModelResource("Taiki"));
	playerObj->SetMoveAlg(playerMove);
	playerObj->SetScale(scale);
	playerObj->SetRotation(angle);
	playerObj->SetPosition(position);
}

void AddObj::SetTerrain(const VECTOR3& scale, const VECTOR3& angle, const VECTOR3& position)
{
	terrainMove = std::make_shared<Terrain>();
	terrainObj = std::make_unique<Obj3D>();
	terrainObj->SetModelResource(LoadModel::GetInstance().GetModelResource("Town"));
	terrainObj->SetMoveAlg(playerMove);
	terrainObj->SetScale(scale);
	terrainObj->SetRotation(angle);
	terrainObj->SetPosition(position);
}
