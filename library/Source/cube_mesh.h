#pragma once

#include <d3d11.h>
#include <wrl.h>
#include <memory>
#include "vector.h"
#include "constant_buffer.h"
#include "texture.h"
#include "shader.h"

using Microsoft::WRL::ComPtr;

class CubeMesh
{
private:
	struct Vertex
	{
		VECTOR3 position;
		VECTOR3 normal;
		VECTOR2 texcoord;
		VECTOR4 color;
	};
	struct Mesh
	{
		ComPtr<ID3D11Buffer> vertexBuffer;
		int numVertices;
		ComPtr<ID3D11Buffer> indexBuffer;
		int numIndices;
	};
	Mesh mesh;

	std::unique_ptr<Texture> texture;
	std::unique_ptr<Shader> shader;

	struct CbScene
	{
		FLOAT4X4	view_projection;
	};
	struct CbMesh
	{
		FLOAT4X4 world;
	};
	struct CbSubset
	{
		VECTOR4	materialColor;
	};
	std::unique_ptr<ConstantBuffer<CbScene>> cbScene;
	std::unique_ptr<ConstantBuffer<CbMesh>> cbMesh;
	std::unique_ptr<ConstantBuffer<CbSubset>> cbSubSet;

	VECTOR3 scale = VECTOR3(1, 1, 1);
	VECTOR3 angle = VECTOR3(0, 0, 0);
	VECTOR3 position = VECTOR3(0, 0, 0);
	FLOAT4X4 world;
	
public:
	CubeMesh(ID3D11Device* device, const wchar_t* filename = nullptr);
	~CubeMesh() {}

	void Update();
	void Render(ID3D11DeviceContext* context, const FLOAT4X4& viewProjection, const VECTOR4& color = VECTOR4(1, 1, 1, 1));
	void SetScale(const VECTOR3& scale) { this->scale = scale; }
	void SetAngle(const VECTOR3& angle) { this->angle = angle; }
	void Setposition(const VECTOR3& position) { this->position = position; }
};