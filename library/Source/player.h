#pragma once

#include <memory>
#include "actor.h"

class Player : public Actor
{
private:

public:
	void Move(Obj3D* obj, float elapsedTime);
};
