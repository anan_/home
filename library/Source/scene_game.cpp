
#include "scene_game.h"
#include "create_DX11.h"
#include "fadeout.h"
#include "load_model.h"

#include "add_obj.h"

#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif

void SceneGame::Init(ID3D11Device* device)
{
	LightInit(device);
	renderer = std::make_unique<ModelRenderer>(device);

	LoadModel& loadModel = LoadModel::GetInstance();
	loadModel.Load(device, "Data/fbx/danbo_fbx/danbo_taiki.fbx", "Taiki");
	loadModel.Load(device, "Data/fbx/fields/town/A_city.fbx", "Town");

	AddObj::Create(device);
	AddObj& add = AddObj::GetInstance();
	add.SetPlayer(VECTOR3(0.1f, 0.1f, 0.1f), VECTOR3(0, 0, 0), VECTOR3(0, 0, 0));

	add.SetTerrain(VECTOR3(0.5f, 0.5f, 0.5f), VECTOR3(0, 0, 0), VECTOR3(0, -10.f, 0));

	Camera& camera = Camera::GetInstance();
	camera.SetEye(VECTOR3(0, 30, 100.f));
	camera.SetFocus(add.GetPlayer()->GetPosition());
}

void SceneGame::Release()
{
	AddObj::Destory();
}

void SceneGame::Update(ID3D11Device* device, float elapsedTime)
{
	AddObj& add = AddObj::GetInstance();
	Camera& camera = Camera::GetInstance();

	add.GetPlayer()->Update(elapsedTime);
}

void SceneGame::Render(ID3D11DeviceContext* context, float elapsedTime)
{
	AddObj& add = AddObj::GetInstance();
	Camera& camera = Camera::GetInstance();

	SetRender::SetBlender(BS_ALPHA);
	SetRender::SetDepthStencilState(DS_TRUE);
	SetRender::SetRasterizerState(RS_CULL_FRONT);

	LightUpdate(context);

	{
		lightBuffer->Activate(context, 3);
		renderer->Begin(context, camera.GetViewProection());

		renderer->Draw(context, add.GetPlayer()->GetModel());

		renderer->End(context);
		lightBuffer->DeActivate(context);
	}
}

void SceneGame::LightInit(ID3D11Device* device)
{
	Light::Init(device);
	lightBuffer = std::make_unique<ConstantBuffer<CbLight>>(device);
}

void SceneGame::LightUpdate(ID3D11DeviceContext* context)
{
	Camera& camera = Camera::GetInstance();

	float XM_PI = 3.141592654f;
	static float lightAngle = XM_PI;
	Light::SetAmbient(VECTOR3(0.5f, 0.5f, 0.5f));
	//ライト方向
	LightDir.x = sinf(lightAngle);
	//でっけえライト
	LightDir.y = 0.0f;
	LightDir.z = cosf(lightAngle);
	static float angle = XM_PI / 4;
	//angle += elapsedTime;
	Light::SetDirLight(LightDir, VECTOR3(1.0f, 1.0f, 1.0f));

	lightBuffer->data.ambientColor = Light::Ambient;
	lightBuffer->data.lightDir = Light::LightDir;
	lightBuffer->data.lightColor = Light::DirLightColor;
	lightBuffer->data.eyePos.x = camera.GetEye().x;
	lightBuffer->data.eyePos.y = camera.GetEye().y;
	lightBuffer->data.eyePos.z = camera.GetEye().z;
	lightBuffer->data.eyePos.w = 1.0f;
}