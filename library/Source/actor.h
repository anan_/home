#pragma once

#include "obj3d.h"
#include "camera.h"

struct ActorParam
{
	VECTOR3 velocity;
	float maxSpeed;
	float acceleration;
	float deceleration;
	float turnSpeed;
};

class Actor : public MoveAlg
{
public:
	Actor();
	void AddForce(const VECTOR3& force) { pram.velocity += force * pram.acceleration; }

protected:
	ActorParam pram;

	VECTOR2 oldCursor, newCursor;

	void SetAcceleration(float acceleration) { pram.acceleration = acceleration; }
	void SetDeceleration(float deceleration) { pram.deceleration = deceleration; }
	void SetMaxSpeed(float maxSpeed) { pram.maxSpeed = maxSpeed; }
	void PlayerMove(Obj3D* obj, float elapsedTime);
	void SetCube(const VECTOR3& scale, const VECTOR3& rotation, const VECTOR3& position);

	void MainMove(Obj3D* obj, float elapsedTime);
};


