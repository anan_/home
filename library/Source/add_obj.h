#pragma once

#include <d3d11.h>
#include <memory>
#include <array>

#include "player.h"
#include "terrain.h"

#include "collision.h"

class AddObj
{
    static AddObj* instance;

private:
    std::unique_ptr<Obj3D> playerObj;
    std::shared_ptr<Player> playerMove;

    std::unique_ptr<Obj3D> terrainObj;
    std::shared_ptr<Terrain> terrainMove;

public:
    AddObj(ID3D11Device* device);
    ~AddObj();

    void SetPlayer(const VECTOR3& scale, const VECTOR3& angle, const VECTOR3& position);
    void SetTerrain(const VECTOR3& scale, const VECTOR3& angle, const VECTOR3& position);
    void RayPick();

    Obj3D* GetPlayer() { return playerObj.get(); }

public:
    static void Create(ID3D11Device* device)
    {
        if (instance != nullptr) return;
        instance = new AddObj(device);
    }
    static AddObj& GetInstance()
    {
        return *instance;
    }
    static void Destory()
    {
        if (instance != nullptr)
        {
            delete instance;
            instance = nullptr;
        }
    }
};

