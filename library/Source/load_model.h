#pragma once

#include "model.h"
#include "model_data.h"
#include "model_resource.h"
#include "fbx_loader.h"
#include <map>

class LoadModel
{
    static LoadModel* instance;
    static std::map<std::string, std::shared_ptr<ModelResource>> modelDatas;

public:
    LoadModel() { }

    // 
    void Load(ID3D11Device* device, const char* filename, const char* modelTag);
    std::shared_ptr<ModelResource>& GetModelResource(const char* modelTag);

    static void Create()
    {
        if (instance != nullptr) return;
        instance = new LoadModel;
    }
    static LoadModel& GetInstance()
    {
        return *instance;
    }
    static void Destory()
    {
        if (instance != nullptr)
        {
            delete instance;
            instance = nullptr;
        }
    }
};

#define pLoadModel LoadModel::GetInstance()
