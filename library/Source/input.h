#pragma once

#include <Windows.h>
#include <xinput.h>

/*
NONE          全部
RISINING_EDGE 押したとき
FALLING_EDGE  放されたとき
*/
enum class TRIGGER_MODE 
{ NONE, RISINING_EDGE, FALLING_EDGE };

class Key
{
    int vkey;
    int curret_state;
    int previous_state;
public:
    Key(int vkey) : vkey(vkey), curret_state(0), previous_state(0)
    {
    }
    virtual ~Key() = default;
    Key(Key &) = delete;
    Key &operator=(Key &) = delete;

    bool state(TRIGGER_MODE trigger_mode)
    {
        previous_state = curret_state;
        if (static_cast<unsigned short>(GetAsyncKeyState(vkey)) & 0x8000)
        {
            curret_state++;
        }
        else
        {
            curret_state = 0;
        }
        if (trigger_mode == TRIGGER_MODE::RISINING_EDGE)
        {
            return previous_state == 0 && curret_state > 0;
        }
        else if (trigger_mode == TRIGGER_MODE::FALLING_EDGE)
        {
            return previous_state > 0 && curret_state == 0;
        }
        else
        {
            return curret_state > 0;
        }
    }
};

