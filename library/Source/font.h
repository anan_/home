#pragma once

#include "sprite.h"

struct LoadFont
{
	int fontNo;//テクスチャ番号
	const wchar_t* filename; //ファイル名

};

enum FontLabel
{
	FONT0 = 0,
	FONT1,
	FONT2,
	FONT3,
	FONT4,
	FONT5,
	FONT6,

	FontMAX
};

class Font 
{
private:
	static Font* instance;
	static const UINT fontNum = FontLabel::FontMAX;
	static const UINT maxInstance = 256;

	std::unique_ptr<SpriteBatch> font[fontNum];

public:
	void Load(ID3D11Device* device);

	void Text(ID3D11DeviceContext* context, int fontNo,
		const VECTOR2& position, const VECTOR2& size,
		const VECTOR4& color,
		const char* str, ...);

	static void Create()
	{
		if (instance != nullptr) return;
		instance = new Font();
	}
	static Font& GetInstance()
	{
		return *instance;
	}
	static void Destory()
	{
		if (instance != nullptr)
		{
			delete instance;
			instance = nullptr;
		}
	}
};

#define pFont Font::GetInstance()
