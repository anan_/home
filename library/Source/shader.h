#pragma once

#include <d3d11.h>
#include <d3dcompiler.h>
#include "vector.h"
#include <Wrl.h>

using Microsoft::WRL::ComPtr;

class Shader
{
protected:
    ComPtr<ID3D11VertexShader> vertex_shader = nullptr;
    ComPtr<ID3D11PixelShader> pixel_shader = nullptr;
    ComPtr<ID3D11GeometryShader> geometric_shader = nullptr;
    ComPtr<ID3D11HullShader> hull_shader = nullptr;
    ComPtr<ID3D11DomainShader> domain_shader = nullptr;

    ComPtr<ID3D11InputLayout> input_layout = nullptr;

public:
    Shader() {/*ZeroMemory(this, sizeof(Shader));*/ }
    virtual ~Shader() { }

    bool CreateSprite(ID3D11Device* device);
	bool CreateSpriteBatch(ID3D11Device* device);
	bool CreateBoard(ID3D11Device* device);
    bool CreateGeometric(ID3D11Device* device);
	bool CreateModel(ID3D11Device* device);
	bool CreateMesh(ID3D11Device* device);

    void Activate(ID3D11DeviceContext* context);
    void DeActivate(ID3D11DeviceContext* context);
};
