#pragma once

#include "sound.h"
#include <memory>

struct LoadSound
{
    int soundNum;
    const char* filename;
};

enum SoundLabel
{
	SE,

	SoundMAX
};

class SoundData
{
	static SoundData* instance;
    const static UINT SOUND_MAX = SoundLabel::SoundMAX;
    std::unique_ptr<Sound> sound[SOUND_MAX];
	SoundData()
	{
	}
	~SoundData() {}

public:
    void Load();
    void Update();
    void Play(int soundNum, bool isLoop = false);
    void Stop(int soundNum);
	void SetVolume(int soundNum, const float volume);

	static void Create()
	{
		if (instance != nullptr) return;
		instance = new SoundData();
	}
	static SoundData& GetInstance()
	{
		return *instance;
	}
	static void Destory()
	{
		if (instance != nullptr)
		{
			delete instance;
			instance = nullptr;
		}
	}
};


