#pragma once

#include "vector.h"
#include <DirectXMath.h>
#include <vector>

class Camera
{
	static Camera* instance;

private:
    // debug
	VECTOR3 eye = VECTOR3(0,0,-50.f);
	VECTOR3 focus = VECTOR3(0,0,0);
	VECTOR3 angle; // yaw, pitch, roll

    VECTOR3 up = VECTOR3(0,1,0);
    VECTOR3 right = VECTOR3(1,0,0);
    VECTOR3 front;

	float distance = 0.0f;
    VECTOR2 oldCursor;
    VECTOR2 newCursor;

    float rotateX = 0.0f;
    float rotateY = 0.0f;

    bool change = true;
    bool moveCursor = false;

	FLOAT4X4 view;
	FLOAT4X4 projection;
	FLOAT4X4 viewProjection;

public:
    Camera();
	~Camera() = default;

public:
	void Update(float elapsedTime);

    void CalculateView();

    void GameCamera(float elapsedTime);
    void DebugCamera();

	// set
    void SetProjection(const float viewAngle,
        const float nearZ, const float farZ);
	void SetEye(const VECTOR3& eye) { this->eye = eye; }
	void SetFocus(const VECTOR3& focus)
	{
		this->focus = focus;
	}

	// get
	FLOAT4X4 SetOrthographic(float w, float h, float znear, float zfar);			//	���s���e�s��ݒ�֐�
	FLOAT4X4 SetPerspective(float fov, float aspect, float znear, float zfar);	//	�������e�s��ݒ�֐�

	const FLOAT4X4 GetView() { return view; };
	const FLOAT4X4 GetProjection() { return	projection; }
	const VECTOR3& GetEye()     const { return eye; }
    const VECTOR3& GetFocus() const { return focus; }
    const VECTOR3& GetFront() const { return front; }
    const VECTOR3& GetRight() const { return right; }
    const VECTOR3& GetUp() const { return up; }
    const bool& GetMode() const { return change; }
	const DirectX::XMMATRIX GetViewMatrix();
	const DirectX::XMMATRIX GetProjectionMatrix();
	const FLOAT4X4 GetViewProection();

public:
    static void Create()
    {
        if (instance != nullptr) return;
        instance = new Camera;
    }
    static Camera& GetInstance()
    {
        return *instance;
    }
    static void Destory()
    {
        if (instance != nullptr)
        {
            delete instance;
            instance = nullptr;
        }
    }
};
