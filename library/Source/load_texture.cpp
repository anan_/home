
#include "load_texture.h"

LoadTexture* LoadTexture::instance = nullptr;

TextureData textureData[LoadTexture::textureMax] =
{
	//  { TextureTag::, L"Data/images/.png", 1U },
	{ TextureTag::MouseImage, L"Data/images/button.png", 1U },

};

LoadTexture::LoadTexture(ID3D11Device* device)
{
	for (int i = 0; i < textureMax; i++)
	{
		sprite[i] = std::make_unique<SpriteBatch>(device, textureData[i].filename, textureData[i].maxInstance);
	}
}

void LoadTexture::RenderLeft(ID3D11DeviceContext* context, int texNo, 
	const VECTOR2& position, const VECTOR2& size, 
	const VECTOR2& texPos, const VECTOR2& texSize,
	float angle,
	const VECTOR4& color,
	bool reverse)
{
	if ((size_t)texNo < textureMax)
	{
		sprite[texNo]->Begin(context);
		sprite[texNo]->RenderLeft(position, size, texPos, texSize, angle, color, reverse);
		sprite[texNo]->End(context);
	}
}

void LoadTexture::RenderCenter(ID3D11DeviceContext* context,int texNo, 
	const VECTOR2& position, const VECTOR2& size, 
	const VECTOR2& texPos, const VECTOR2& texSize, 
	const VECTOR2& center, 
	float angle, const
	VECTOR4& color,
	bool reverse)
{
	if ((size_t)texNo < textureMax)
	{
		sprite[texNo]->Begin(context);
		sprite[texNo]->RenderCenter(position, size, texPos, texSize, center, angle, color, reverse);
		sprite[texNo]->End(context);
	}
}
