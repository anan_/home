
#include "scene_title.h"

#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif

void SceneTitle::Init(ID3D11Device* device)
{
}

void SceneTitle::Release()
{
}

void SceneTitle::Update(ID3D11Device* device, float elapsedTime)
{
	}

void SceneTitle::Render(ID3D11DeviceContext* context, float elapsedTime)
{
}