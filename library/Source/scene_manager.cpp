
#include "scene_manager.h"

#include "scene_title.h"
#include "scene_game.h"
#include "scene_clear.h"
#include "scene_over.h"

#include "mouse.h"
#include "font.h"
#include "load_model.h"
#include "sound_data.h"
#include "load_texture.h"

#include "camera.h"

#include "fadeout.h"

#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif

SceneManager* SceneManager::instance = nullptr;
std::map<std::string, std::unique_ptr<Scene>> SceneManager::scenes;

SceneManager::SceneManager(HINSTANCE hInstance, Create_DX11* _create_dx11, ID3D11Device* device) : create_dx11(_create_dx11)
{
    scenes[ToSTRING(TITLE)] = std::make_unique<SceneTitle>();
    scenes[ToSTRING(GAME)] = std::make_unique<SceneGame>();
    scenes[ToSTRING(CLEAR)] = std::make_unique<SceneClear>();
    scenes[ToSTRING(OVER)] = std::make_unique<SceneOver>();

    Mouse::Create();
    SoundData::Create();
    SoundData::GetInstance().Load();
    LoadTexture::Create(device);
    FadeOut::GetInstance().Create(device);

    LoadModel::Create();
    Camera::Create();
    current_scene = ToSTRING(GAME);
    scenes[current_scene]->Init(device);
}

void SceneManager::Execute(HINSTANCE hInstance, ID3D11DeviceContext* context, ID3D11Device* device)
{
    while (create_dx11->GameLoop())
    {
        //input::GamepadUpdate();
        Mouse::GetInstance().Update();
        FadeOut::GetInstance().ChangeScene(device, create_dx11->GetElapsedTime());
        Camera::GetInstance().Update(create_dx11->GetElapsedTime());
        scenes[current_scene]->Update(device, create_dx11->GetElapsedTime());

        create_dx11->RenderingBegin();
        Mouse::GetInstance().Render();
        scenes[current_scene]->Render(context, create_dx11->GetElapsedTime());
        FadeOut::GetInstance().Render(context);
        create_dx11->RenderingEnd();

        const float average_rendering_time = create_dx11->GetAverageRenderingTime();

#ifdef USE_IMGUI
        ImGui::Begin("SceneManager");

        static float time = 0.f;
        time += create_dx11->GetElapsedTime();

        const char* scenes[] = { "TITLE", "GAME", "CLEAR" };
        static int current = 0;
        ImGui::Combo("NextScene", &current, scenes, IM_ARRAYSIZE(scenes));

        if (ImGui::Button("SceneSet"))
        {
            switch (current)
            {
            case TITLE:
                ChangeScene(device, ToSTRING(TITLE));
                break;

            case GAME:
                ChangeScene(device, ToSTRING(GAME));
                break;

            case CLEAR:
                ChangeScene(device, ToSTRING(CLEAR));
                break;
            }

            FadeOut::GetInstance().SetScene(scenes[current]);
        }

        ImGui::End();
#endif // USE_IMGUI


    }
}

void SceneManager::ChangeScene(ID3D11Device* device, const char* next_scene)
{
    scenes[current_scene]->Release();

    current_scene = next_scene;
    scenes[current_scene]->Init(device);

    create_dx11->ResetHighResolutionTimer();
}

void SceneManager::Release()
{
    Camera::Destory();
    LoadModel::Destory();
    FadeOut::Destory();
    LoadTexture::Destory();
    SoundData::Destory();
    Mouse::Destory();

    //scenes[current_scene]->Release();
    scenes[ToSTRING(TITLE)]->Release();
    scenes[ToSTRING(GAME)]->Release();
    scenes[ToSTRING(CLEAR)]->Release();
    scenes[ToSTRING(OVER)]->Release();
}
