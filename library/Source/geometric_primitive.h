#pragma once

#include <d3d11.h>
#include <wrl.h>
#include <memory>
#include <vector>
#include "vector.h"
#include "constant_buffer.h"
#include "texture.h"
#include "shader.h"

using Microsoft::WRL::ComPtr;

class GeometricPrimitive
{
protected:
	struct Vertex
	{
		VECTOR3 position;
		VECTOR3 normal;
	};
	struct Mesh
	{
		ComPtr<ID3D11Buffer> vertexBuffer;
		int numVertices = 0;
		ComPtr<ID3D11Buffer> indexBuffer;
		int numIndices = 0;
	};
	Mesh mesh;

	std::vector<Vertex> vertices;
	std::vector<unsigned int> indices;
    
	std::unique_ptr<Shader> shader;

	struct CbScene
	{
		FLOAT4X4	view_projection;
	};
	struct CbMesh
	{
		FLOAT4X4 world;
	};
	struct CbSubset
	{
		VECTOR4	materialColor;
	};
	std::unique_ptr<ConstantBuffer<CbScene>> cbScene;
	std::unique_ptr<ConstantBuffer<CbMesh>> cbMesh;
	std::unique_ptr<ConstantBuffer<CbSubset>> cbSubSet;

	VECTOR3 scale = VECTOR3(1, 1, 1);
	VECTOR3 angle = VECTOR3(0, 0, 0);
	VECTOR3 position = VECTOR3(0, 0, 0);
	FLOAT4X4 world;

    bool	CreateBuffers(ID3D11Device* device,
        Vertex* vertices, int numV,
        unsigned int* indices, int numI);

	bool	CreateBuffers(ID3D11Device* device,
		const std::vector<Vertex>& vertices,
		const std::vector<unsigned int>& indices);

protected:
    GeometricPrimitive()
    {}

public:
    GeometricPrimitive(ID3D11Device* device)
    {
		cbScene = std::make_unique<ConstantBuffer<CbScene>>(device);
		cbMesh = std::make_unique<ConstantBuffer<CbMesh>>(device);
		cbSubSet = std::make_unique<ConstantBuffer<CbSubset>>(device);

		shader = std::make_unique<Shader>();
		shader->CreateGeometric(device);

		world = FLOAT4X4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
    }

	virtual ~GeometricPrimitive() {}

	virtual void Update();
	virtual void Render(ID3D11DeviceContext* context, const FLOAT4X4& viewProjection, const VECTOR4& color = VECTOR4(1.f, 1.f, 1.f, 1.f));
	void SetScale(const VECTOR3& scale) { this->scale = scale; };
	void SetAngle(const VECTOR3& angle) { this->angle = angle; }
	void SetPosition(const VECTOR3& position) { this->position = position; }

};

class GeometricRect : public GeometricPrimitive
{
public:
	GeometricRect(ID3D11Device* device);
	~GeometricRect() {};
};


class GeometricBoard : public GeometricPrimitive
{
public:
	GeometricBoard(ID3D11Device* device);
	~GeometricBoard() {};
};


class GeometricCube : public GeometricPrimitive
{
public:
	GeometricCube(ID3D11Device* device);
	~GeometricCube() {};
};


class GeometricSphere : public GeometricPrimitive
{
public:
	GeometricSphere(ID3D11Device* device, int slices = 16, int stacks = 16);
	~GeometricSphere() {};
};


//class GeometricSphere2 : public GeometricPrimitive
//{
//private:
//	int		numVertices;
//	inline Vertex _makeVertex(const DirectX::XMFLOAT3& p);
//public:
//	//	形状は slices = (div-1) * 4, stacks = (div-1) * 2 と同じ
//	GeometricSphere2(ID3D11Device* device, u_int div = 8);
//	~GeometricSphere2() {};
//	
//	virtual void render(ID3D11DeviceContext* context,
//		const DirectX::XMFLOAT4X4& wvp,
//		const DirectX::XMFLOAT4X4& world,
//		const DirectX::XMFLOAT4& light_direction,
//		const DirectX::XMFLOAT4& material_color = DirectX::XMFLOAT4(1, 1, 1, 1),
//		bool bSolid = true);
//};
