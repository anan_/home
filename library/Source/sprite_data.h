#pragma once

#include "create_DX11.h"
using namespace GetCoreSystem;
#include "vector.h"
#include "load_texture.h"
#include "collision.h"

class SpriteData
{
	TextureTag textureTag = TextureTag::Max;
	VECTOR2 texPos;
	VECTOR2 texSize;
	VECTOR2 center;

	VECTOR2 size;
	bool reverse = false;

	Collision::Rect rect;
	VECTOR2 hitPosition;
	VECTOR2 hitSize;

public:
	void RendeCenter(
		const VECTOR2& position, const VECTOR2& scale,
		float angle = 0.f,
		const VECTOR4& color = VECTOR4(1, 1, 1, 1),
		bool reverse = false)
	{
		size = VECTOR2(texSize.x * scale.x, texSize.y * scale.y);
		center = size / 2;

		rect.position = hitPosition;
		rect.size = hitSize;

		LoadTexture::GetInstance().RenderCenter(GetContext(), static_cast<int>(textureTag),
			position, size,
			texPos, texSize,
			center,
			angle,
			color,
			reverse);
	}

	void RenderLeft(
		const VECTOR2& position,
		float angle = 0.f,
		const VECTOR4& color = VECTOR4(1, 1, 1, 1),
		bool reverse = false)
	{
		const VECTOR2& _texPos = texPos;
		const VECTOR2& _texSize = texSize;

		LoadTexture::GetInstance().RenderLeft(
			GetContext(), static_cast<int>(textureTag),
			position, size,
			_texPos, _texSize,
			angle,
			color,
			reverse);
	}

	void SetTexture(TextureTag tag,
		const VECTOR2& texPos, const VECTOR2& texSize)
	{
		textureTag = tag;
		this->texPos = texPos;
		this->texSize = texSize;
		this->center = texSize / 2;
	}

	void SetHitRect(const VECTOR2& position, const VECTOR2& scale)
	{
		hitSize = VECTOR2(texSize.x * scale.x, texSize.y * scale.y);
		hitPosition = position - hitSize / 2;
	}
	void SetHitPosition(const VECTOR2& position) { hitPosition = position - hitSize / 2; }
	void SetHitScale(const VECTOR2& scale) { hitSize = VECTOR2(texSize.x * scale.x, texSize.y * scale.y); }

	const VECTOR2& GetSize() const { return size; }
	const VECTOR2& GetTexSize() const { return texSize; }
	const VECTOR2& GetCenter() const { return center; }
	const Collision::Rect& GetRect()  const { return rect; }
};

struct AnimeData
{
	SpriteData* data;
	float frame;//表示時間
};

struct Anime
{
	float frame;//表示時間
	int patNum;//パターン番号
	int animcount; //アニメーション切り替わり回数
	AnimeData* animed;//前回のアニメーションデータ
};
