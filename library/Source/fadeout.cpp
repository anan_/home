
#include "fadeout.h"
#include "scene_manager.h"
#include "window.h"

FadeOut* FadeOut::instance = nullptr;

bool FadeOut::Update(float elapsedTime)
{
	if (!moveFlag) return false;

	colorW += elapsedTime;
	if (colorW >= 1.f)
	{
		colorW = 0.f;
		moveFlag = false;
		return true;
	}

	return false;
}

void FadeOut::ChangeScene(ID3D11Device* device, float elapsedTime)
{
	if (!moveFlag || nextScene == nullptr) return;

	colorW += elapsedTime;
	if (colorW >= 1.f)
	{
		colorW = 0.f;
		moveFlag = false;
		SceneManager::GetInstance().ChangeScene(device, nextScene);
	}

	return;
}

void FadeOut::MoveStart(const char* nextScene)
{
	this->nextScene = nextScene;
	moveFlag = true;
}

void FadeOut::Render(ID3D11DeviceContext* context)
{
	if (!moveFlag) return;
	fade->Render(context, VECTOR2(0, 0), VECTOR2(SCREEN_WIDTH, SCREEN_HEIGHT),
		VECTOR2(0, 0), VECTOR2(1024, 1024), colorW);
}
