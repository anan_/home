#pragma once

#include "scene.h"
#include "constant_buffer.h"
#include "model_renderer.h"
#include "light.h"

class SceneGame : public Scene
{
private:
	std::unique_ptr<ModelRenderer> renderer;

	struct CbLight
	{
		VECTOR4 lightColor;
		VECTOR4 lightDir;
		VECTOR4 ambientColor;
		VECTOR4 eyePos;
		POINTLIGHT PointLight[Light::POINTMAX];
		SPOTLIGHT SpotLight[Light::SPOTMAX];
	};
	std::unique_ptr<ConstantBuffer<CbLight>> lightBuffer;
	VECTOR3 LightDir;

	void LightInit(ID3D11Device* device);
	void LightUpdate(ID3D11DeviceContext* context);

public:
	void Init(ID3D11Device* device);
	void Release();
	void Update(ID3D11Device* device, float elapsedTime);
	void Render(ID3D11DeviceContext* context, float elapsedTime);
};